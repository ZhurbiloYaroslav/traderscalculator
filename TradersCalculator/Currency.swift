//
//  Currency.swift
//  TradersCalculator
//
//  Created by Yaroslav Zhurbilo on 16.07.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import Foundation

//TODO: Make these variables not global, but in the Instruments.swift like an instance variables

let AUD = "AUD"
let CAD = "CAD"
let CHF = "CHF"
let GBP = "GBP"
let EUR = "EUR"
let NZD = "NZD"
let SGD = "SGD"
let USD = "USD"
let JPY = "JPY"
let DKK = "DKK"
let HKD = "HKD"
let NOK = "NOK"
let PLN = "PLN"
let SEK = "SEK"
let TRY = "TRY"
let ZAR = "ZAR"
let CNH = "CNH"
let CZK = "CZK"
let HUF = "HUF"
let MXN = "MXN"
let RUB = "RUB"
let THB = "THB"
let XBR = "XBR"
let XNG = "XNG"
let XTI = "XTI"
let BTC = "BTC"


//TODO: Write a description
let instrumentPartFullName = [
    
    AUD : "Australian Dollar",
    CAD : "Canadian Dollar",
    CHF : "Swiss Franc",
    GBP : "Great Britain Pound",
    EUR : "Euro",
    NZD : "New Zealand Dollar",
    SGD : "Singapore Dollar",
    USD : "US Dollar",
    JPY : "Japanese Yen",
    DKK : "Danish Korner",
    HKD : "Hong Kong Dollar",
    NOK : "Norwegian Krona",
    PLN : "Polish Zlotys",
    SEK : "Swedish Kronor",
    TRY : "Turkish Lira",
    ZAR : "South African Rand",
    CNH : "Chinese Renminbi",
    CZK : "Czech Republic Korunas",
    HUF : "Hungarian Forints",
    MXN : "Mexican Peso",
    RUB : "Russian Ruble",
    THB : "Thai Baht",
    XBR : "Brent",
    XNG : "Natural Gas",
    XTI : "Ml",
    BTC : "Bitcoin"
    
]
